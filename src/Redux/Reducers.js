import { createStore } from 'redux'

const initialState = { session: false, authorize: false, loader: false, token: null, userinfo: {} }

const reducer = (state = initialState, action) => {
    state = localStorage.getItem('redux-state')
    state = state ? JSON.parse(state) : initialState
    switch(action.type) {
        case 'session/login':
            state.session = true
            state.authorize = false
            state.token = action.payload
            break
        case 'session/logout':
            state.session = false
            state.authorize = false
            state.token = null
            state.userinfo = {}
            break
        case 'session/authorize':
            state.authorize = true
            state.userinfo = action.payload
            break
        case 'session/deauthorize':
            state.authorize = false
            state.userinfo = {}
            break
        case 'loader/show': 
            state.loader = true
            break
        case 'loader/hide': 
            state.loader = false
            break
        default:
            return state
    }
    localStorage.setItem('redux-state', JSON.stringify(state))
    return state
}

const store = createStore(reducer)

export default store