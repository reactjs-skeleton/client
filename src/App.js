import React from 'react'
import { Provider } from 'react-redux'
import { ApolloClient, InMemoryCache, ApolloProvider, createHttpLink } from '@apollo/client'
import { setContext } from '@apollo/client/link/context';
import qs from 'querystring'
import store from './Redux/Reducers'
import env from './Config/env'
import Router from './Components/Router'
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles'
import {SnackbarProvider} from 'notistack'

import Middleware from './Components/Middleware/Middleware'

export class App extends React.Component {
    constructor(props) {
        super(props)
        let self = this
        let states = store.getState()
        store.subscribe(() => {
            const updatedStates = store.getState()
            if(updatedStates.token !== states.token) {
                states = updatedStates
                self.forceUpdate()
            }
        })
    }
    render() {
        const __baseurl = `http://${env.hostname}/${env.api}/graphql`
        const httpLink = createHttpLink({ uri: __baseurl })
        const authLink = setContext((_, { headers }) => {
            const authorization = qs.stringify({token: store.getState().token})
            return { headers: { ...headers, authorization } }
        })
        const client = new ApolloClient({ 
            link: authLink.concat(httpLink), cache: new InMemoryCache(), errorPolicy: 'all' 
        })

        const theme = createMuiTheme({
            typography: {
                fontFamily: ['Helvetica','sans-serif'].join(','),
                color: '#fff',
                body1: {
                    fontSize: 14
                }
            },
            overrides: {
                MuiTableCell: {
                    root: {
                        padding: '10px'
                    },
                    head: {
                        fontWeight: 'bold'
                    }
                },
                MuiTableHead: {
                    root: {
                        borderTop: '2px solid lightgrey'
                    }
                },
                MuiToolbar: {
                    root: {
                        minHeight: 0
                    }
                },
                MuiDialog: {
                    paper: {
                        borderRadius: '5px',
                    }
                },
                MuiDialogTitle: {
                    root: {
                        padding: '16px 24px 0px 24px',
                        borderBottom: '2px solid darkgrey'
                    }
                },
                MuiDialogActions: {
                    root: {
                        padding: '10px 24px 24px 24px',
                    }
                },
                MuiDialogContent: {
                    root: {
                        padding: '0px 24px',
                        paddingBottom: '10px'
                    }
                }
            }
        })
        
        return ( 
            <ApolloProvider client={client}>
                <Middleware>
                    <ThemeProvider theme={theme}>
                        <Provider store={store}> 
                            <SnackbarProvider maxSnack={3}>
                                <Router /> 
                            </SnackbarProvider>
                        </Provider> 
                    </ThemeProvider>
                </Middleware>
            </ApolloProvider>
        )
    }
}

export default App