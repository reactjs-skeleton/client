import React from 'react'
import loginImg from '../../Images/Login.svg'
import store from '../../Redux/Reducers'
import { makeStyles } from '@material-ui/core/styles'
import { gql, useMutation } from '@apollo/client'
import { Redirect } from 'react-router-dom'

const useStyles = makeStyles({
    container: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '1em auto',
        maxWidth: '350px',
        padding: '30px 10px',
        border: '4px solid lightgrey',
        borderRadius: '4px',
    },
    header: {
        fontSize: '24px',
        fontFamily: ['Open Sans', 'sans-serif'],
    },
    content: {
        display: 'flex',
        flexDirection: 'column',
    },
    contentImage: {
        width: '21em',
    },
    contentImageElement: {
        width: '100%',
        height: '100%',
    },
    form: {
        marginTop: '2em',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    formGroup: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex start',
        width: 'fit-content',
    },
    formGroupLabel: {
        fontSize: '20px',
    },
    formGroupInput: {
        marginTop: '6px',
        minWidth: '18em',
        height: '37px',
        padding: '0px 10px',
        fontSize: '16px',
        fontFamily: ['Open Sans', 'sans-serif'],
        backgroundColor: '#f3f3f3',
        border: '0px',
        borderRadius: '4px',
        marginBottom: '31px',
        transition: 'all 250ms ease-in-out',
        '&:focus': {
            outline: 'none',
            boxShadow: '0px 0px 12px 0.8px #0e81ce96',
        }
    },
    footer: {
        marginTop: '1em',
    },
    btn: {
        fontSize: '21px',
        padding: '5px 20px',
        border: '0',
        backgroundColor: '#3498db',
        color: '#fff',
        borderRadius: '3px',
        transition: 'all 250ms ease-in-out',
        cursor: 'pointer',
        '&:hover': {
            backgroundColor: '#2386c8'
        },
        '&:focus': {
            outline: 'none'
        }
    }
})

const Login = (props) => {
    const classes = useStyles()
    const [username, setUsername] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [loginError, setLoginError] = React.useState('')

    const mutation = gql`
        mutation authenticate($username: String!, $password: String!) {
            authenticate(username: $username, password: $password) 
        }
    `
    const [request] = useMutation(mutation)

    const authenticate = async (e) => {
        e.preventDefault()
        await request({variables: {username, password}}).then(async (response) => {
            const {token} = response.data.authenticate
            store.dispatch({type: 'session/login', payload: token})
        }).catch((error) => {
            setLoginError(error.message)
        })
    }

    if(store.getState().session) {
        return <Redirect to="/admin/dashboard" />
    }

    return (
        <div className={classes.container} ref={props.containerRef}>
            <div className={classes.header}>
                Login
            </div>
            <form onSubmit={authenticate}>
                <div className={classes.content}>
                    <div className={classes.contentImage}>
                        <img className={classes.contentImageElement} src={loginImg} alt="test"/>
                    </div>
                    <div className={classes.form}>
                        <div className={classes.formGroup}>
                            <label className={classes.formGroupLabel} htmlFor="username">Username</label>
                            <input 
                                className={classes.formGroupInput} 
                                type="text" 
                                name="username" 
                                placeholder="Please input username" 
                                onChange={e => setUsername(e.target.value)} 
                                value={username}/>
                        </div>
                        <div className={classes.formGroup}>
                            <label className={classes.formGroupLabel} htmlFor="password">Password</label>
                            <input 
                                className={classes.formGroupInput} 
                                type="password" 
                                name="password" 
                                placeholder="Please input password" 
                                onChange={e => setPassword(e.target.value)} 
                                value={password}/>
                        </div>
                        {loginError}
                    </div>
                </div>
                <div className={classes.footer}>
                    <button type="submit" className={classes.btn}>
                        Login
                    </button>
                </div>
            </form>
        </div>
    )
}

export default Login