import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow } from '@material-ui/core'
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from '@material-ui/core'
import { IconButton, Button, Box, TextField } from '@material-ui/core'
import { gql, useQuery, useMutation } from '@apollo/client'
import { Edit, Delete, Add } from '@material-ui/icons'
import { useSnackbar } from 'notistack'
import { Autocomplete } from '@material-ui/lab'
import Header from '../Helper/Header'

const columns = [
    { id: 'package_name', label: 'Package', minWidth: 170 },
    { id: 'provider_name', label: 'Provider', minWidth: 170 },
    { id: 'purchase', label: 'Purchase', minWidth: 100 },
    { id: 'sell', label: 'Sell', minWidth: 100 },
]

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: 440,
    },
})

export default function PackagesList() {
    const classes = useStyles()
    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(10)

    const getPackagesQuery = gql`{packages{ get }}`
    const packagesData = useQuery(getPackagesQuery)

    const getProviderQuery = gql`{provider{ get }}`
    const providerData = useQuery(getProviderQuery)

    const [openAddDialog, setOpenAddDialog] = React.useState(false)
    const [openEditDialog, setOpenEditDialog] = React.useState(false)
    const [editPackageId, setEditPackageId] = React.useState(null)

    const AddPackagesForm = () => {
        const [providerId, setProviderId] = React.useState(null)
        const [packageName, setPackageName] = React.useState('')
        const [purchase, setPurchase] = React.useState('')
        const [sell, setSell] = React.useState('')
    
        const {enqueueSnackbar} = useSnackbar()
    
        const AddPackagesMutation = gql`
            mutation addPackages($package_name: String!, $provider_id: String!, $purchase: Int!, $sell: Int!) {
                packages {
                    create(package_name: $package_name, provider_id: $provider_id, purchase: $purchase, sell: $sell)
                }
            }
        `
        const [AddPackagesRequest] = useMutation(AddPackagesMutation)
    
        const action = async (e) => {
            e.preventDefault()
            AddPackagesRequest({variables: {
                provider_id: providerId.id, 
                package_name: packageName, 
                purchase: parseInt(purchase), 
                sell: parseInt(sell)
            }}).then((response) => {
                setOpenAddDialog(false)
                enqueueSnackbar('Packages is successfully added.', {variant: 'success'})
                packagesData.refetch()
            }).catch((e) => {
                setOpenAddDialog(false)
                console.log(e)
            })
        }
        return (
            <Dialog open={openAddDialog} onClose={() => setOpenAddDialog(false)} aria-labelledby="form-dialog-title">
                <form onSubmit={action}>
                    <DialogTitle id="form-dialog-title">Add Package Data</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To add package data, please fill all of form input here.
                        </DialogContentText>
                        <Autocomplete
                            id="provider_id"
                            options={!providerData.loading ? providerData.data.provider.get : []}
                            getOptionLabel={(option) => option.provider_name}
                            renderInput={(params) => <TextField {...params} label="Provider Name" />}
                            onChange={(e, val) => { setProviderId(val) }}
                            value={providerId}/>
                        <TextField
                            onChange={e => setPackageName(e.target.value)}  
                            id="package_name" label="Package Name" type="text" fullWidth
                            value={packageName}/>
                        <TextField
                            onChange={e => setPurchase(e.target.value)}  
                            id="purchase" label="Purchase Price" type="number" fullWidth
                            value={purchase}/>
                        <TextField
                            onChange={e => setSell(e.target.value)}  
                            id="sell" label="Sell Price" type="number" fullWidth
                            value={sell}/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setOpenAddDialog(false)} variant="outlined" color="default" size="small">
                            Cancel
                        </Button>
                        <Button type="submit" variant="outlined" color="primary" size="small">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        )
    }

    const EditPackagesForm = () => {
        const editedValue = packagesData.data.packages.get.filter((el) => el.id === editPackageId)[0]
        const editedProvider = providerData.data.provider.get.filter((el) => el.id === editedValue.provider_id)[0]
        const [providerId, setProviderId] = React.useState(editedProvider)
        const [packageName, setPackageName] = React.useState(editedValue.package_name)
        const [purchase, setPurchase] = React.useState(editedValue.purchase)
        const [sell, setSell] = React.useState(editedValue.sell)
    
        const {enqueueSnackbar} = useSnackbar()
    
        const EditPackagesMutation = gql`
            mutation editPackages($package_id: String!, $package_name: String!, $provider_id: String!, $purchase: Int!, $sell: Int!) {
                packages {
                    update(package_id: $package_id, package_name: $package_name, provider_id: $provider_id, purchase: $purchase, sell: $sell)
                }
            }
        `
        const [EditPackagesRequest] = useMutation(EditPackagesMutation)

        const action = async (e) => {
            e.preventDefault()
            EditPackagesRequest({variables: {
                package_id: editPackageId,
                provider_id: providerId.id, 
                package_name: packageName, 
                purchase: parseInt(purchase), 
                sell: parseInt(sell)
            }}).then((response) => {
                setOpenEditDialog(false)
                enqueueSnackbar('Packages is successfully updated.', {variant: 'success'})
                packagesData.refetch()
            }).catch((e) => {
                setOpenEditDialog(false)
                console.log(e)
            })
        }
        return (
            <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)} aria-labelledby="form-dialog-title">
                <form onSubmit={action}>
                    <DialogTitle id="form-dialog-title">Edit Package Data</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To add package data, please fill all of form input here.
                        </DialogContentText>
                        <Autocomplete
                            id="provider_id"
                            options={!providerData.loading ? providerData.data.provider.get : []}
                            getOptionLabel={(option) => option.provider_name}
                            renderInput={(params) => <TextField {...params} label="Provider Name" />}
                            onChange={(e, val) => { setProviderId(val) }}
                            value={providerId}/>
                        <TextField
                            onChange={e => setPackageName(e.target.value)}  
                            id="package_name" label="Package Name" type="text" fullWidth
                            value={packageName}/>
                        <TextField
                            onChange={e => setPurchase(e.target.value)}  
                            id="purchase" label="Purchase Price" type="number" fullWidth
                            value={purchase}/>
                        <TextField
                            onChange={e => setSell(e.target.value)}  
                            id="sell" label="Sell Price" type="number" fullWidth
                            value={sell}/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setOpenEditDialog(false)} variant="outlined" color="default" size="small">
                            Cancel
                        </Button>
                        <Button type="submit" variant="outlined" color="primary" size="small">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        )
    }

    return (
        <Paper className={classes.root}>
            <Header 
                title="List of Package" 
                breadcrumb={[
                    {display: 'Monitoring', active: true},
                    {display: 'Package', active: true}
                ]}>
                    <Box component="div" textAlign="right">
                        <IconButton fontSize="small" onClick={() => setOpenAddDialog(true)}>
                            <Add /> 
                        </IconButton>
                    </Box>
            </Header>
            <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                            <TableCell key="action" align="center">Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {!packagesData.loading ? 
                            packagesData.data.packages.get.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                                return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                                        {columns.map((column) => {
                                            const value = row[column.id]
                                            return (
                                                <TableCell key={column.id} align={column.align}>
                                                    {column.format && typeof value === 'number' ? column.format(value) : value}
                                                </TableCell>
                                            )
                                        })}
                                        <TableCell key="action">
                                            <IconButton variant="outlined" color="primary" size="small" onClick={() => {
                                                setEditPackageId(row.id)
                                                setOpenEditDialog(true)
                                            }}>
                                                <Edit />
                                            </IconButton>
                                            <IconButton variant="outlined" color="secondary" size="small">
                                                <Delete />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                )
                            }) : null
                        }
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={!packagesData.loading ? packagesData.data.packages.get.length : 0}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={(event, newPage) => {
                    setPage(newPage)
                }}
                onChangeRowsPerPage={(event) => {
                    setRowsPerPage(+event.target.value)
                    setPage(0)
                }}
            />
            <AddPackagesForm />
            {openEditDialog ? <EditPackagesForm /> : null}
        </Paper>
    )
}