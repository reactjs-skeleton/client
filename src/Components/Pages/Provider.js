import React from 'react'
import Header from '../Helper/Header'
import { Box } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import MaterialTable from 'material-table';
import MaterialTableIcons from '../Helper/MaterialTableIcons'
import {gql, useQuery, useMutation} from '@apollo/client'
import { useSnackbar } from 'notistack'

const useStyles = makeStyles({
    root: {
        marginTop: '10px',
        "& .MuiPaper-root": {
            padding: '0px 15px'
        }
    }
})

export default function Provider (props) {
    const classes = useStyles()
    const {enqueueSnackbar} = useSnackbar()
    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Provider Name', field: 'provider_name' },
    ])
    
    const getProviderQuery = gql`{provider{ get }}`
    const {loading, data, refetch} = useQuery(getProviderQuery)
    const editableData = !loading ? data.provider.get.map(o => ({ ...o })) : []

    const addProviderMutation = gql`
        mutation addProvider($provider_name: String!) {
            provider {
                create(provider_name: $provider_name)
            }
        }
    `
    const updateProviderMutation = gql`
        mutation updateProvider($id: String!, $provider_name: String!) {
            provider {
                update(id: $id, provider_name: $provider_name)
            }
        }
    `
    const deleteProviderMutation = gql`
        mutation deleteProvider($id: String!) {
            provider {
                delete(id: $id)
            }
        }
    `
    const [addProviderRequest] = useMutation(addProviderMutation)
    const [updateProviderRequest] = useMutation(updateProviderMutation)
    const [deleteProviderRequest] = useMutation(deleteProviderMutation)

    return (
        <Box component="div">
            <Header 
                title="Provider"
                breadcrumb={[
                    {display: 'Monitoring', active: true},
                    {display: 'Provider', active: true}
                ]}/>
            <Box component="div" className={classes.root}>
                <MaterialTable
                    title=""
                    columns={columns}
                    data={editableData}
                    icons={MaterialTableIcons}
                    options={{
                        actionsColumnIndex: 2,
                        headerStyle: {
                            borderTop: '2px solid lightgrey',
                            fontWeight: 'bold'
                        }
                    }}
                    editable={{
                        onRowAdd: async (newData) => {
                            addProviderRequest({variables: newData}).then((response) => {
                                enqueueSnackbar('Provider is successfully added.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                        onRowUpdate: async (newData, oldData) => {
                            updateProviderRequest({variables: {...newData, id: oldData.id}}).then((response) => {
                                enqueueSnackbar('Provider is successfully updated.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                        onRowDelete: async (oldData) => {
                            deleteProviderRequest({variables: {id: oldData.id}}).then((response) => {
                                enqueueSnackbar('Provider is successfully deleted.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                    }}
                />
            </Box>
        </Box>
    )
}