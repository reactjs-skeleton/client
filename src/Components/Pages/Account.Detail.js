import React from 'react'
import Header from '../Helper/Header'
import { Box, Grid, makeStyles, TextField, Typography, Button } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import { useParams } from 'react-router-dom'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import defaultDP from '../../Images/avatar_male.png'

const useStyles = makeStyles(theme => ({
    content: {
        marginLeft: '5px',
        padding: '15px 5px',
    },
    form: {
        margin: '10px 0px',
        padding: '8px 10px',
        border: '1px solid lightblue',
        borderLeft: '3px solid blue',
        '& .title': {
            fontWeight: 'bold',
            fontSize: '1rem'
        },
        '& .action-container': {
            marginTop: '10px',
            width: '100%',
            textAlign: 'right'
        }
    },
    picture: {
        textAlign: 'center',
        padding: '20px 10px 10px 10px',
        '& .profile-picture': {
            maxWidth: '150px',
            padding: '0px 20px',
            borderRadius: '100px'
        }
    }
}))

export default function Provider (props) {
    const classes = useStyles()
    const { username } = useParams()

    const formik = useFormik({
        initialValues: {
            username: ''
        },
        validationSchema: Yup.object({
            username: Yup.string().required()
        }),
        onSubmit: values => {

        }
    })

    const formAccountData = useFormik({
        initialValues: {
            name: '',
            address: '',
        },
        validationSchema: Yup.object({
            name: Yup.string().required(),
            address: Yup.string().required(),
        }),
        onSubmit: values => {
            
        }
    })

    const formRoles = useFormik({
        initialValues: {
            roles: [],
        },
        validationSchema: Yup.object({
            roles: Yup.array().required('Roles is required.'),
        }),
        onSubmit: values => {
            
        }
    })
    

    return (
        <Box component="div">
            <Header 
                title="Account Management"
                breadcrumb={[
                    {display: 'Super Admin', active: false, url: '/admin/account'},
                    {display: 'Account Management', active: false, url: '/admin/account'},
                    {display: username, active: true},
                ]} />
            <Box className={classes.content}>
                <Grid container spacing={3}>
                    <Grid item md={4} sm={12}>
                        <Box className={classes.form}>
                            <Typography className="title">Edit Profile Picture</Typography>
                            <form>
                                <Box className={classes.picture}>
                                    <img 
                                        className='profile-picture' 
                                        src={defaultDP} 
                                        alt="Default Profile" />
                                    <TextField
                                        helperText={formik.errors.username}
                                        margin="dense"
                                        onChange={formik.handleChange}
                                        id="username" label="Email" type="file" fullWidth
                                        value={formik.values.username}/>
                                </Box>
                                <Box className="action-container">
                                    <Button type="submit" variant="contained" color="primary">
                                        Save Password
                                    </Button>
                                </Box>
                            </form>
                        </Box>
                    </Grid>
                    <Grid item md={8} sm={12}>
                        <Box className={classes.form}>
                            <Typography className="title">Edit Account Data</Typography>
                            <form onSubmit={formAccountData.handleSubmit}>
                                <TextField
                                    helperText={formAccountData.errors.name}
                                    margin="dense"
                                    onChange={formAccountData.handleChange} 
                                    id="name" label="Full Name" type="text" fullWidth
                                    value={formAccountData.values.name}/>
                                <TextField
                                    helperText={formAccountData.errors.address}
                                    margin="dense"
                                    onChange={formAccountData.handleChange}
                                    id="address" label="Address" type="text" fullWidth
                                    value={formAccountData.values.address}/>
                                <Box className="action-container">
                                    <Button type="submit" variant="contained" color="primary">
                                        Save Account Data
                                    </Button>
                                </Box>
                            </form>
                        </Box>
                        <Box className={classes.form}>
                            <Typography className="title">Edit Roles</Typography>
                            <form onSubmit={formik.handleSubmit}>
                                <TextField
                                    helperText={formik.errors.username}
                                    margin="dense"
                                    onChange={formik.handleChange}
                                    id="username" label="Email" type="text" fullWidth
                                    value={formik.values.username}/>
                                <Box className="action-container">
                                    <Button type="submit" variant="contained" color="primary">
                                        Save Roles
                                    </Button>
                                </Box>
                            </form>
                        </Box>
                        <Box className={classes.form}>
                            <Typography className="title">Change Password</Typography>
                            <form onSubmit={formik.handleSubmit}>
                                <TextField
                                    helperText={formik.errors.username}
                                    margin="dense"
                                    onChange={formik.handleChange}
                                    id="username" label="Email" type="text" fullWidth
                                    value={formik.values.username}/>
                                <Box className="action-container">
                                    <Button type="submit" variant="contained" color="primary">
                                        Save Password
                                    </Button>
                                </Box>
                            </form>
                        </Box>
                        <Box className={classes.form}>
                            <Typography className="title">Change Email</Typography>
                            <form onSubmit={formik.handleSubmit}>
                                <TextField
                                    helperText={formik.errors.username}
                                    margin="dense"
                                    onChange={formik.handleChange}
                                    id="username" label="Email" type="text" fullWidth
                                    value={formik.values.username}/>
                                <Box className="action-container">
                                    <Button type="submit" variant="contained" color="primary">
                                        Save Email
                                    </Button>
                                </Box>
                            </form>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    )
}