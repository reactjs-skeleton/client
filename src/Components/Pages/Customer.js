import React from 'react'
import Header from '../Helper/Header'
import { Box } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import MaterialTable from 'material-table';
import MaterialTableIcons from '../Helper/MaterialTableIcons'
import {gql, useQuery, useMutation} from '@apollo/client'
import { useSnackbar } from 'notistack'

const useStyles = makeStyles({
    root: {
        marginTop: '10px',
        "& .MuiPaper-root": {
            padding: '0px 15px'
        }
    }
})

export default function Provider (props) {
    const classes = useStyles()
    const {enqueueSnackbar} = useSnackbar()
    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Phone Number', field: 'phone' },
        { title: 'Name', field: 'name' },
        { title: 'Email Address', field: 'email' },
        { title: 'Address', field: 'address' },
    ])
    
    const getCustomerQuery = gql`{customer{ get }}`
    const {loading, data, refetch} = useQuery(getCustomerQuery)
    const editableData = !loading ? data.customer.get.map(o => ({ ...o })) : []

    const addCustomerMutation = gql`
        mutation addCustomer($name: String!, $phone: String!, $email: String!, $address: String!) {
            customer {
                create(name: $name, phone: $phone, email: $email, address: $address)
            }
        }
    `
    const updateCustomerMutation = gql`
        mutation updateCustomer($id: String!, $name: String!, $phone: String!, $email: String!, $address: String!) {
            customer {
                update(id: $id, name: $name, phone: $phone, email: $email, address: $address)
            }
        }
    `
    const deleteCustomerMutation = gql`
        mutation deleteCustomer($id: String!) {
            customer {
                delete(id: $id)
            }
        }
    `
    const [addCustomerRequest] = useMutation(addCustomerMutation)
    const [updateCustomerRequest] = useMutation(updateCustomerMutation)
    const [deleteCustomerRequest] = useMutation(deleteCustomerMutation)

    return (
        <Box component="div">
            <Header 
                title="Customer"
                breadcrumb={[
                    {display: 'Monitoring', active: true},
                    {display: 'Customer', active: true}
                ]}/>
            <Box component="div" className={classes.root}>
                <MaterialTable
                    title=""
                    columns={columns}
                    data={editableData}
                    icons={MaterialTableIcons}
                    options={{
                        actionsColumnIndex: 5,
                        headerStyle: {
                            borderTop: '2px solid lightgrey',
                            fontWeight: 'bold'
                        }
                    }}
                    editable={{
                        onRowAdd: async (newData) => {
                            addCustomerRequest({variables: newData}).then((response) => {
                                enqueueSnackbar('Customer data is successfully added.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                        onRowUpdate: async (newData, oldData) => {
                            updateCustomerRequest({variables: {...newData, id: oldData.id}}).then((response) => {
                                enqueueSnackbar('Customer data is successfully updated.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                        onRowDelete: async (oldData) => {
                            deleteCustomerRequest({variables: {id: oldData.id}}).then((response) => {
                                enqueueSnackbar('Customer data is successfully deleted.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                    }}
                />
            </Box>
        </Box>
    )
}