import React from 'react'
import Header from '../Helper/Header'
import { Box } from '@material-ui/core'

export default (props) => {
    return (
        <Box component="div">
            <Header title="Dashboard" subtitle="General Statistic Overview" />
        </Box>
    )
}