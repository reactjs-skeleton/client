import React from 'react'
import store from '../../Redux/Reducers'
import {gql, useQuery} from '@apollo/client'

const Middleware = (props) => {
    const userinfoQuery = gql`{ userinfo }`
    const { loading } = useQuery(userinfoQuery, { onCompleted: (response) => {
        store.dispatch({type: 'session/authorize', payload: response.userinfo})
    }, onError: (e) => {
        console.log(e.message)
        store.dispatch({type: 'session/logout'})
    }} )
    return (<div>{loading ? null : props.children}</div>)
}

export default Middleware