import React from 'react'
import { Route, withRouter, Redirect } from 'react-router-dom'
import Admin from '../Layout/Admin'
import store from '../../Redux/Reducers'

export class Routes extends React.Component {
    render(props) {
        const Component = withRouter(this.props.component)
        const Theme = this.props.theme === 'admin' ? Admin : null
        const state = store.getState()
        if(state.session && state.authorize) {
            if(Theme) {
                return (<Route path={this.props.path} render={() => <Theme {...this.props} />} />)
            } else {
                return <Route path={this.props.path} render={() => Component}></Route>
            }
        } else {
            return <Route path={this.props.path} render={() => <Redirect to='/login' />} />
        }
    }
}
export default withRouter(Routes)