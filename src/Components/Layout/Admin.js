import React from 'react'
import { Redirect } from 'react-router-dom'
import { Drawer, Grid, Box, AppBar, Toolbar, Typography, Button, Menu, MenuItem, List, ListItem, ListItemText, ListItemIcon, Divider, Hidden } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { Settings, Menu as MenuIcon } from './Icons'
import * as Icons from './Icons'
import defaultDP from '../../Images/avatar_male.png'
import store from '../../Redux/Reducers'
import env from '../../Config/env'

const useStyles = makeStyles({
    /** Header */
    header: {
        zIndex: '1250',
    },
    toolbar: {
        minHeight: '44px'
    },
    brand: {
        display: 'flex',
        flex: 1,
    },
    brandText: {
        paddingTop: '5px',
        fontSize: '1.2rem',
        width: '210px',
    },
    menuIcon: {
        color: 'white',
    },
    rightIcon: {
        color: 'white',
    },

    /** Sidebar */
    display: {
        textAlign: 'center',
        marginTop: '30px',
        padding: '20px 10px 10px 10px',
    },
    displayRole: {
        fontSize: '0.8rem',
        fontWeight: 'bold'
    },
    displayPicture: {
        width: '90px',
        height: '90px',
        padding: '10px',
        borderRadius: '100px'
    },
    sidebar: {
        width: '240px',
        transform: 'none',
    },
    sidebarhide: {
        transition: 'transform 200ms cubic-bezier(2, 0, 0, 0) 200ms',
        transform: 'translateX(-240px)',
    },
    labelMenu: {
        fontSize: '12px',
        padding: '3px 10px',
        color: 'darkgray',
    },
    list: {
        paddingLeft: '4px'
    },
    listItem: {
        padding: '4px 16px',
        '&.active': {
            borderLeft: '3px solid blue',
            background: 'lightblue'
        }
    },
    listItemIcon: {
        minWidth: '40px',
    },
    listItemText: {
        fontSize: '0.8rem',
        paddingTop: '4px',
        color: '#000'
    },

    /** Content */
    main: {
        display: 'flex',
        width: '100%',
        flexDirection: 'row',
        marginTop: '35px',
        '& .MuiDrawer-paper': {
            width: '240px'
        }
    },
    content: {
        width: '100%',
        padding: '5px',
    },
    footer: {
        bottom: '0',
        left: 'auto',
        right: '0',
        position: 'fixed',
        zIndex: '1250',
        backgroundColor: '#EEEEEE',
        width: '100%',
        height: '35px',
        paddingTop: '10px',
        '& .footer-text': {
            padding: '0px 20px',
            fontWeight: 'bold'
        }
    }
})

export default (props) => {
    const classes = useStyles()
    const state = store.getState()

    const [sidebar, setSidebar] = React.useState(true)
    const [anchor, setAnchor] = React.useState(null)

    // let find = false
    // const destination = window.location.pathname
    // state.userinfo.menu.forEach(row => {
    //     row.menu.forEach(nestRow => {
    //         if(nestRow.url === destination) find = true
    //     })
    // })

    // if(!find) return <Redirect to={state.userinfo.menu[0].menu[0].url} />

    const Display = () => {
        return (
            <Box className={classes.display}>
                <img 
                    className={classes.displayPicture} 
                    src={state.userinfo.photo ? `data:image/jpeg;base64,${state.userinfo.photo}`: defaultDP} 
                    alt="Default Profile" />
                <Typography>{state.userinfo.name}</Typography>
                <Typography className={classes.displayRole}>Administrator</Typography>
            </Box>
        )
    }

    const ListMenu = () => {
        const pathname = window.location.pathname
        return (
            <List className={classes.list}>
                {state.userinfo.menu.map((list) => {
                    return (
                        <span key={list.label}>
                            <Typography className={classes.labelMenu}>{list.label}</Typography>
                            {list.menu.map((item) => {
                                const ListIcon = Icons[item.icons]
                                return (
                                    <ListItem 
                                        className={item.url === pathname ? 'active':''} 
                                        classes={{root: classes.listItem}} 
                                        key={item.url}
                                        onClick={() => props.history.push(item.url)}
                                        button>
                                        <ListItemIcon classes={{root: classes.listItemIcon}}><ListIcon /></ListItemIcon>
                                        <ListItemText classes={{root: classes.listItemText}} primary={item.title} />
                                    </ListItem>
                                )
                            })}
                        </span>
                    )
                })}
            </List>
        )
    }

    const Footer = () => {
        return (
            <Box className={classes.footer}>
                <Typography className="footer-text">
                    Copyright &copy; 2020 Moch. Annafia O. All right reserved.
                </Typography>
            </Box>
        )
    }

    return (
        <Grid container direction="row">
            <AppBar classes={{root: classes.header}}>
                <Toolbar classes={{root: classes.toolbar}}>
                    <Box className={classes.brand}>
                        <Typography className={classes.brandText}>
                            {env.brand}
                        </Typography>
                        <Button onClick={() => { setSidebar(!sidebar) }}>
                            <MenuIcon className={classes.menuIcon} />
                        </Button>
                    </Box>
                    <Button aria-controls="simple-menu" aria-haspopup="true" onClick={(e) => setAnchor(e.currentTarget)}>
                        <Settings className={classes.rightIcon} />
                    </Button>
                    <Menu 
                        id="simple-menu"
                        keepMounted
                        anchorEl={anchor}
                        open={Boolean(anchor)}
                        onClose={() => setAnchor(null)}
                        classes={{paper: classes.rightMenu}}>
                        <MenuItem onClick={() => setAnchor(null)}>My Account</MenuItem>
                        <MenuItem onClick={() => setAnchor(null)}>Logout</MenuItem>
                    </Menu>
                </Toolbar>
            </AppBar>
            <Box className={classes.main}>
                <Hidden xsDown>
                    <Drawer
                        classes={{ root: sidebar ? classes.sidebar : classes.sidebarhide }}
                        variant="persistent"
                        open={sidebar} >
                        <Display /> 
                        <Divider /> 
                        <ListMenu />
                    </Drawer>
                </Hidden>
                <Hidden smUp>
                    <Drawer
                        classes={{ 
                            root: !sidebar ? classes.sidebar : classes.sidebarhide,
                            paper: classes.paper
                        }}
                        variant='temporary'
                        open={!sidebar} >
                            <AppBar>
                                <Toolbar classes={{root: classes.toolbar}}>
                                    <Box className={classes.brand}>
                                        <Typography className={classes.brandText}>
                                            {env.brand}
                                        </Typography>
                                        <Button onClick={() => { setSidebar(!sidebar) }}>
                                            <MenuIcon className={classes.menuIcon} />
                                        </Button>
                                    </Box>
                                    <Button onClick={() => { setSidebar(!sidebar) }}>
                                        <Settings className={classes.rightIcon} />
                                    </Button>
                                </Toolbar>
                            </AppBar>
                            <Display /> 
                            <Divider /> 
                            <ListMenu />
                            <Footer />
                    </Drawer>
                </Hidden>
                <div className={classes.content}>
                    {props.children}
                </div>
            </Box>
            <Footer />
        </Grid>
    )
}