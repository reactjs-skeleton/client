import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import PrivateRoute from './Middleware/PrivateRoute'
import Dashboard from './Pages/Dashboard'
import Provider from './Pages/Provider'
import Packages from './Pages/Packages'
import Customer from './Pages/Customer'
import Account from './Pages/Account'
import AccountDetail from './Pages/Account.Detail'
import LandingPage from './Pages/LandingPage'
import Login from './Pages/Login'

export default class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Route exact path='/' component={ LandingPage } />
                <Route path='/login' component={ Login } />
                <PrivateRoute path='/admin' component={ Dashboard } theme = 'admin'>
                    <Switch>
                        <Route path='/admin/dashboard' component={ Dashboard } />
                        <Route path='/admin/provider' component={ Provider } />
                        <Route path='/admin/package' component={ Packages } />
                        <Route path='/admin/customer' component={ Customer } />
                        <Route exact path='/admin/account' component={ Account } />
                        <Route exact path='/admin/account/:username' component={ AccountDetail } />
                    </Switch>
                </PrivateRoute>
            </BrowserRouter>
        )
    }
}